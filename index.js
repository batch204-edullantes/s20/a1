let number = Number(prompt('Enter number:'));

if(isNaN(number)) {
	alert('Your input is not a number!');
} else {
	console.log('The number you provided is ' + number);
	for (let i = number; i >= 0; i--) {
		if(i <= 50) {
			console.log('The current value is at 50. Terminating the loop.');
			break;
		}

		if((i % 10) == 0) {
			console.log('The number is divisible by 10. Skipping the number.');
			continue;
		}

		if((i % 5) == 0) {
			console.log(i);
		}
	}
}


let word = 'supercalifragilisticexpialidocious';
let stringConsonants = '';
let vowels = ['a', 'e', 'i', 'o', 'u']

for (let i = 0; i < word.length; i++) {
	if(vowels.includes(word[i])) {
		continue;
	} else {
		stringConsonants += word[i];
	}
}
console.log(word);
console.log(stringConsonants);